# Getting Started with My Calendar App

This project utilizes a basic React app to check the availability of 2 calendars, and generates available time slots for a meeting.

The project utilizes dummy data, so to run it, simply clone the repo locally, install the node dependencies, and run the app using `yarn start` as listed below.

### Change Dummy Data

To test out different calendar combinations, simply edit lines 10-13 in the MeetingAvailability component under `src/components`

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Notes

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).