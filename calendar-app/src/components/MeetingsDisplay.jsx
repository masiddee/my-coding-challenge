import React, { useState } from 'react';
import moment from 'moment';
import Button from '@material-ui/core/Button';

/** ==================================== */
/**
 * UPDATE THE CALENDARS & BOUNDS HERE
 */

const calendar1 = [['12:00', '13:00'], ['16:00', '18:00']];
const dailyBounds1 = [['8:30', '20:30']]
const calendar2 = [['10:00', '11:30'],['12:30', '14:30'], ['14:30', '15:00'], ['16:00', '17:00']];
const dailyBounds2 = [['9:00', '18:30']];
const meetingDuration = 30;
/** ==================================== */

const MeetingsDisplay = () => {
  const [ availability, updateAvailability ] = useState([]);

  /** Helper function to generate all available meeting windows */
  const generateAllWindows = (start, end, duration) => {
    const windows = [];
  
    do {
      windows.push(start);
      start = start + duration;
    } while (start < end);
  
    return windows;
  }

  /** Convert calendar into moment.duration() minutes to run calculations on them */
  const convertCal = (cal) => {
    return cal.map(elem => {
      const start = moment.duration(elem[0]).asMinutes();
      const end = moment.duration(elem[1]).asMinutes();
  
      return { start, end }
    });
  }

  /**
   * Moment doesn't allow for reformatting durations: https://github.com/moment/moment/issues/463
   * Had to create a custom function to format the moment.duration() to military time.
   */
  const convertCalToMilitary = (cal) => {
    return cal.map(elem => {
      let start = (elem.start / 60).toString();
      let end = (elem.end / 60).toString();

      if (start.split('.').length > 1) {
        start = start.split('.')[0] + ':30';
      } else {
        start = start + ':00';
      }

      if (end.split('.').length > 1) {
        end = end.split('.')[0] + ':30';
      } else {
        end = end + ':00';
      }

      return { start, end }
    })
  }

  const checkAvailability = (c1, c2, b1, b2, duration) => {
    const cal1 = convertCal(c1);
    const cal2 = convertCal(c2);
    const bounds1 = convertCal(b1);
    const bounds2 = convertCal(b2);

    /** Get latest start and earliest end time between both bounds. */
    const startBound = bounds1[0].start > bounds2[0].start ? bounds1[0].start : bounds2[0].start;
    const endBound = bounds1[0].end < bounds2[0].end ? bounds1[0].end : bounds2[0].end;

    /** Generate all possible meeting windows based on the combined daily bounds. */
    const allWindows = generateAllWindows(startBound, endBound, duration);
    const availableMeetings = [];

    /**
     * Loop through all meeting windows and compare against both calendars.
     * Push any available windows to availableMeetings array.
     */
    for (let i = 0; i < allWindows.length; i++) {
      const isCal1Busy = cal1.some(_ => (allWindows[i] >= _.start && allWindows[i] < _.end));
      const isCal2Busy = cal2.some(_ => (allWindows[i] >= _.start && allWindows[i] < _.end));
      console.log({avail: allWindows[i], isCal1Busy, isCal2Busy})

      if (!isCal1Busy && !isCal2Busy) {
        availableMeetings.push({ start: allWindows[i], end: allWindows[i] + duration });
      }
    }
    
    /** Raw output for reference. */
    console.log({ startBound, endBound, cal1, cal2, allWindows, avail: convertCalToMilitary(availableMeetings) })

    return updateAvailability(convertCalToMilitary(availableMeetings));
  }

  return (
    <div>
      <h2>Click button to Check availability</h2>
      <ul>
        {
          availability.map(elem => <li>{ elem.start } - { elem.end }</li>)
        }
      </ul>
      <Button
        variant="contained"
        color="primary"
        size="large"
        onClick={ () => checkAvailability(calendar1, calendar2, dailyBounds1, dailyBounds2, meetingDuration) }>Check Availability</Button>
    </div>
  );
}

export default MeetingsDisplay;
