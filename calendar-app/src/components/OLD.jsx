import React from 'react';
import moment from 'moment';

const calendar1 = [['12:00', '13:00'], ['16:00', '18:00']];
const dailyBounds1 = [['8:30', '20:30']]
const calendar2 = [['10:00', '11:30'],['12:30', '14:30'], ['14:30', '15:00'], ['16:00', '17:00']];
const dailyBounds2 = [['8:00', '18:30']];
const meetingDuration = 30;

const convertCal = (cal) => {
  const newCal = cal.map(elem => {
    const start = moment.duration(elem[0]).asMinutes();
    const end = moment.duration(elem[1]).asMinutes();

    return { start, end }
  })

  return newCal;
}

const MeetingsDisplayOLD = (props) => {
  const getCalAvailability = (cal, bounds) => {
    const newCal = convertCal(cal);
    const newBounds = convertCal(bounds);
    const calAvail = [];

    // If the first meeting starts later than the bounds start, record this availability in cal1Avail
    if (newCal[0].start > newBounds[0].start) {
      calAvail.push({start: newBounds[0].start, end: newCal[0].start})
    }

    // Loop over each meeting entry and record any availability 
    newCal.forEach((meeting, i, arr) => {
      if (arr[i+1]) {
        if (meeting.end !== arr[i+1].start) {
          calAvail.push({start: meeting.end, end: arr[i+1].start})
        }
      }
    });

    // If the last meeting ends earlier than the bounds end, record this availability in cal1Avail
    if(newCal[newCal.length-1].end < newBounds[0].end) {
      calAvail.push({start: newCal[newCal.length-1].end, end: newBounds[0].end})
    }

    return calAvail;
  }

  const checkAvailability = (cal1, cal2, bounds1, bounds2, duration) => {
    const cal1Availability = getCalAvailability(cal1, bounds1);
    const cal2Availability = getCalAvailability(cal2, bounds2);

    const availableMeetingTimes = [];

    cal1Availability.forEach(avail1 => {
      cal2Availability.forEach(avail2 => {
        if (avail1.end > avail2.start && avail1.start < avail2.end) {
          const mtgStart = avail1.start > avail2.start ? avail1.start : avail2.start;
          const mtgEnd = avail1.end < avail2.end ? avail1.end : avail2.end;

          if (mtgEnd - mtgStart !== duration) {
            let time = mtgEnd - mtgStart;
            let s = mtgStart;

            do {
              availableMeetingTimes.push({ mtgStart: s, mtgEnd: s + duration });
              s = s + duration;
              time = time - duration;
            } while (time >= duration)
          } else {
            availableMeetingTimes.push({ mtgStart, mtgEnd });
          }
        }
      });
    });

    console.log({ cal1Availability, cal2Availability, availableMeetingTimes });
  }

  return (
    <div>
      <h1 onClick={ () => checkAvailability(calendar1, calendar2, dailyBounds1, dailyBounds2, meetingDuration) }>TEST</h1>
    </div>
  );
}

export default MeetingsDisplayOLD;
