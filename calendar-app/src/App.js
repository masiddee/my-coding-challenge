import MeetingsDisplay from './components/MeetingsDisplay';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MeetingsDisplay />
      </header>
    </div>
  );
}

export default App;
